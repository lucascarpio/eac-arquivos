var crypto = require('crypto');

module.exports = function (app) {
    var Schema = require('mongoose').Schema,

        user = new Schema({
            firstName: {type: String, required: true},
            lastName: {type: String, required: false},
            photo: {type: String, required: false},
            name: {
                type: String,
                required: true,
                index: {unique: true}
            },
            email: {
                type: String,
                required: true,
                index: {unique: true}
            },
            password: {
                type: String,
                required: true,
                min: 6,
                set: function (value) {
                    return hash(value);
                }
            },
            lastModified: {type: Date, required: true, default: Date.now},
            status: {type: Number, enum: [0, 1, 2], default: 0}
        });

    var hash = function createHash(value) {
        var md5 = crypto.createHash('md5');
        return md5.update(value).digest('hex');
    };

    user.methods.getPhotoThumbs = function (size) {
        if (this.photo) {
            return this.photo.replace(".", "_" + size + ".");
        } else {
            return "/img/user_48.png";
        }
    };

    user.methods.isValidPassword = function (passwordString) {
        return this.password === hash(passwordString);
    };

    return db.model('user', user);
};

/*
 {
 "firstName" : "Admin",
 "lastName" : "Admin",
 "name" : "admin",
 "password" : "aa1bf4646de67fd9086cf6c79007026c",
 "email" : "admin@ecxample.com"
 }
 */