var dateFormat = require("../dateFormat");

module.exports = function (app) {
    var Schema = require('mongoose').Schema;
    var meeting = new Schema({
        theme: {type: String, required: true},
        date: {
            type: Date, required: true, get: function (val) {
                return val.toISOString().substr(0, 10);
            }, index: {unique: true}
        },
        presence: [Schema.Types.ObjectId]
    });

    meeting.methods.formatDate = function (pattern) {
        return dateFormat(this.date.replace(/-/g, "/"), pattern);
    };

    return db.model('meeting', meeting);
};