var crypto = require('crypto'),
    md5 = crypto.createHash('md5');

module.exports = function (app) {
    var Schema = require('mongoose').Schema;
    var teen = new Schema({
        name: {type: String, required: true},
        photo: {type: String, required: false},
        email: {
            type: String,
            required: true,
            index: {unique: true}
        },
        fatherName: String,
        motherName: String,
        phone: {type: String, required: true},
        address: {type: String, required: true},
        dateOfBirth: {type: Date, required: true},
        registrationNumber: {type: Number, min: 1, max: 9999},
        greatMeetingNumber: {type: Number, min: 1, max: 99}
    });

    teen.methods.getPhotoThumbs = function (size) {
        if (this.photo) {
            return this.photo.replace(".", "_" + size + ".");
        } else {
            return "/img/user_48.png";
        }
    };

    return db.model('teen', teen);
};