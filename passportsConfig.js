var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;

passport.use('login', new LocalStrategy(
    function (username, password, done) {

        User.findOne({$or: [{name: username}, {email: username}]}, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, {message: 'Nome incorreto.'});
            }
            if (!user.isValidPassword(password)) {
                return done(null, false, {message: 'Senha incorreta.'});
            }
            return done(null, user);
        });
    }
));

passport.use('signup', new LocalStrategy({
        passReqToCallback: true
    },
    function (req, usernae, password, done) {
        findOrCreateUser = function () {
            User.findOne({name: username}, function (err, user) {
                if (err) {
                    console.log('Erro no Registro: ' + err);
                    return done(err);
                }
                if (user) {
                    console.log('Usuário já existe');
                    return done(null, false,
                        req.flash('message', 'Usuário já existe'));
                } else {
                    var newUser = new User();
                    newUser.name = username;
                    newUser.password = createHash(password);
                    newUser.email = req.param('email');
                    newUser.firstName = req.param('firstName');
                    newUser.lastName = req.param('lastName');

                    newUser.save(function (err) {
                        if (err) {
                            console.log('Erro ao salvar usuário: ' + err);
                            throw err;
                        }
                        console.log('Registro de usuário com sucesso');
                        return done(null, newUser);
                    });
                }
            });
        };
        process.nextTick(findOrCreateUser);
    }));

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

module.exports = passport;