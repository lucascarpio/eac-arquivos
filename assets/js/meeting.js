$("#search-teen").autoComplete({
    minLength: 4, onEnter: function () {
        $('.presence-roll-actived i').remove();
        var el = $('#presence-roll').children().first();

        $('.presence-roll-actived').append(el);
        $('.modal-content .presence-roll-actived > div').unbind('click').click(onClickCard);
    },
    url: function () {
        var el = $("#search-teen"), url = el.data('url') + el.val(), ids = $('.modal-content .presence-roll-actived input');
        for (var i = 0; i < ids.length; i++)
            url += url.indexOf('?') == -1 ? '?excludes=' + ids[i].value : '&excludes=' + ids[i].value;
        return url;
    },
    onShow: function () {
        $('#presence-roll >  div').click(function (ev) {
            var el = $(this);
            el.unbind('click').click(onClickCard);
            $('.presence-roll-actived').append(el);
        });
    }
});

$('.modal-content .presence-roll-actived > div').click(onClickCard);


function onClickCard(ev) {
    var r = confirm($(this).find('label').text() + " não está presente?");
    if (r) {
        $(this).remove();
        $('.presence-roll-actived > div input[value="' + $(this).find('input').val() + '"]').closest('div').remove();
    }
}

$('#myModal').on('hidden.bs.modal', function () {
    $('[type=submit]').focus();
});

$('#myModal').on('shown.bs.modal', function () {
    $('#search-teen').focus();
});

