$.fn.autoComplete = function (configs) {
    var LOADING = 0, READY = 1, DEFAULT = 2, el = this, trigger = undefined, container = $(el.data('container')), inputTarget = undefined;
    configs.status = configs.status || READY;
    configs.minLength = configs.minLength || 3;
    configs.blackLayer = configs.blackLayer || false;
    configWatch(configs);

    configs.watch('status', function (id, oldval, newval) {
        switch (newval) {
            case 0:
                setTimeout(function () {
                    container.html("<li class='loader' style='text-align: center;'>\
						      <img style='height: 50px;width: 50px;' src='/img/loading.gif'/>\
						   </li>").show();
                    $('.content-busca').addClass('content-busca-focus');
                    if (configs.blackLayer) {
                        $('.black_layer').remove();
                        container.after('<div class="black_layer" style="background-color:black;position:fixed;width:100%;height:100%;z-index:555;opacity:0.7"></div>');
                    }
                }, 0);
                break;
            case 1:
                setTimeout(function () {
                    container.find('.loader').remove();
                    if (configs.onShowOptions) {
                        configs.onShowOptions();
                    }
                    container.find('.option').on('click', function (ev) {
                        var opTarget = $(ev.target);
                        inputTarget.val(opTarget.text());
                        if (configs.selectOption) {
                            configs.selectOption(opTarget, inputTarget);
                        }
                        configs.status = DEFAULT;
                    });
                }, 0);
                break;
            case 2:
                setTimeout(function () {
                    container.html('').hide();
                    $('.black_layer').remove();
                    $('.content-busca').removeClass('content-busca-focus');
                }, 0);
        }
        return newval;
    });

    $(el).on("keyup", function (ev) {
        if (ev.keyCode == 13) {
            $(this).parent('form').submit();
            if (configs.onEnter) {
                configs.onEnter();
            }
            return;
        }
        inputTarget = $(ev.target);
        if (trigger) {
            clearTimeout(trigger);
        }

        trigger = setTimeout(function () {
            if (inputTarget.val().length >= configs.minLength) {
                configs.status = LOADING;
                $.ajax({
                    url: configs.url !== undefined ? configs.url() : inputTarget.data("url") + inputTarget.val(),
                    cache: false,
                    success: function (data) {
                        if (data && data.length > 0) {
                            container.html(data).show();
                            configs.status = READY;
                            if(configs.onShow)configs.onShow();
                        } else {
                            configs.status = DEFAULT;
                        }
                    },
                    error: function (err) {
                        console.log(err);
                        container.html("<i>Ocorreu um erro</i>").show();
                    }
                });
            } else {
                configs.status = DEFAULT;
            }
        }, 300);
    });
    return this;
};