$.fn.fieldValidate = function (configs) {
    var isReadyToSend = true;
    $(this).on('blur', function (e) {
        var formGp = $(this).closest(".form-group"), message = undefined;
        formGp.removeClass("has-error").removeClass("has-success").find(".message").remove();
        if (this.value != "") {
            switch (this.name) {
                case 'password':
                    if (!/((?=.*\d)(?=.*[a-z])(?=.*[!@#$%&*+]).{6,20})/.test(this.value)) {
                        message = "A senha deve conter entre 8 a 20 caracteres, sendo letras, números e caracteres especiais.";
                    }
                    $("[name=confirmPassword]").val("");
                    break;
                case 'confirmPassword':
                    if (this.value != $("#password").val()) {
                        message = "Senha não confere";
                    }
                    break;

            }

            switch (this.type) {
                case 'email':
                    if (!/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil| biz|info|mobi|name|aero|asia|jobs|museum)\b/.test(this.value)) {
                        message = "Formato de e-mail desconhecido";
                    }
                    break;
                case 'tel':
                    $(this).attr('maxlength', '15');
                    var v = $(this).val();
                    v = v.replace(/\D/g, "");
                    v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
                    v = v.replace(/(\d)(\d{4})$/, "$1-$2");
                    $(this).val(v);
            }

            if (message) {
                $(this).after("<label class='message'>" + message + "</label>");
                formGp.addClass("has-error");
            } else {
                formGp.addClass("has-success");
            }
            sendCondition();
        }
    });

    function sendCondition() {
        $(this).closest("form").find("input:required").each(function (index) {
            if (this.value == "") isReadyToSend = false;
        });
        if ($(this).closest("form").find(".has-error").size() == 0 && isReadyToSend)
            $("[type=submit]").removeAttr("disabled");
        else
            $("[type=submit]").attr("disabled", "disabled");
    }

    sendCondition();
};
$('form.toValidate input').fieldValidate({});