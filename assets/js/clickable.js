$('.clickable').on('click', function (e) {
    var el = $(this);
    window.location.href = el.data("click-action");
});

$('.copy-value').on('blur', function (e) {
    var el = $(this), inputRef = "#" + el.data("copy-to");
    $(inputRef).val(el.val());
});