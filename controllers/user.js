var compressAndResizeImage = require("../image-compress");

module.exports = {
    index: function (req, res) {
        User.find(function (err, users) {
            res.render('users/list', {
                users: users,
                error: req.flash('error'),
                success: req.flash('success'),
                currentPage: "usuarios"
            });
        });
    },
    show: function (req, res) {
        User.findById(req.params.id, function (err, user) {
            res.render('users/register', {user: user, currentPage: "usuarios"});
        });
    },
    new: function (req, res) {
        res.render('users/register', {currentPage: "usuarios"});
    },
    save: function (req, res) {
        if (req.files.photo) {
            req.body.photo = req.files.photo.path.replace("public", "");
            compressAndResizeImage(req.files.photo.path);
        }
        var user = new User(req.body);
        User.findOneAndUpdate({email: req.body.email}, user, {upsert: true}, function (err, user) {
            if (err) {
                console.log(err);
                req.flash("error", err.message);
                res.redirect('/usuarios');
            } else {
                req.flash("success", "Usuário salvo com sucesso!");
                res.redirect('/usuarios');
            }
        });
    }
};