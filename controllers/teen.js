var compressAndResizeImage = require("../image-compress");

const ACCENT_STRINGS = 'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ';

const NO_ACCENT_STRINGS = 'SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy';

function str_split(string, split_length) {
    var pos = 0;
    var chunks = [];
    var len = string.length;

    if (!split_length) {
        split_length = 1;
    }
    if (!string || split_length < 1) {
        return false;
    }
    string += '';

    while (pos < len) {
        chunks.push(string.slice(pos, pos += split_length));
    }

    return chunks;
}

function accentToRegex(text) {

    var from = str_split(decodeURIComponent(ACCENT_STRINGS));

    var to = str_split(NO_ACCENT_STRINGS.toLowerCase());

    text = decodeURIComponent(text);

    var regex = {};

    for (var key in to) {
        if (regex[to[key]]) {
            regex[to[key]].push(from[key]);
        } else {
            regex[to[key]] = [];
            regex[to[key]].push(to[key]);
        }
    }

    for (var rg_key in regex) {
        text = text.replace(new RegExp(rg_key, "gi"), "[" + regex[rg_key] + "]");
    }

    return decodeURIComponent(text);

}

module.exports = {
    index: function (req, res) {
        Teenager.find({}, null, {sort: {'name': 1}}, function (err, teens) {
            res.render('teens/list', {
                teens: teens,
                error: req.flash('error'),
                success: req.flash('success'),
                currentPage: "adolescentes"
            });
        });
    },
    show: function (req, res) {
        Teenager.findById(req.params.id, function (err, teen) {
            var date = teen.dateOfBirth.toISOString().substr(0, 10);
            res.render('teens/register', {teen: teen, date: date, currentPage: "adolescentes"});
        });
    },
    new: function (req, res) {
        res.render('teens/register', {currentPage: "adolescentes"});
    },
    save: function (req, res) {
        if (req.files.photo) {
            req.body.photo = req.files.photo.path.replace("public", "");
            compressAndResizeImage(req.files.photo.path);
        }
        Teenager.count({greatMeetingNumber: req.body.greatMeetingNumber}, function (err, count) {
            req.body.registrationNumber = req.body.greatMeetingNumber + count;
            var teen = new Teenager(req.body);
            Teenager.findOneAndUpdate({email: req.body.email}, teen, {upsert: true}, function (err, teen) {
                if (err) {
                    console.log(err);
                    req.flash("error", err.message);
                }
                else req.flash("success", "Salvo com sucesso!");
                res.redirect('/adolescentes');
            });
        });
    },
    getByTerm: function (req, res) {
        var numberExpression = /^[0-9]*$/g,
            query = (numberExpression.test(req.params.term) ? {
                registrationNumber: req.params.term
            } : {name: new RegExp(accentToRegex(req.params.term), 'i')});
        if (req.query.excludes)
            query._id = typeof req.query.excludes === 'string' ? {$ne: req.query.excludes} : {$nin: req.query.excludes};//terminar!

        Teenager.find(query, null, {limit: 10}, function (err, teens) {
            if (err) {
                console.log(err);
                res.render('teens/teen-cards', {error: "Ocorreu um erro"});
            } else res.render('teens/teen-cards', {teens: teens});
        });
    }
};
