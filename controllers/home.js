var crypto = require('crypto'),
    fs = require('fs'),
    md5 = crypto.createHash('md5');
module.exports = {
    login: function (req, res) {
        res.render('home/login', {error: req.flash('error'), currentPage: "login"});
    },
    index: function (req, res) {
        User.findById(req.session.passport.user, function (err, user) {
            if (err) req.flash('error', err);
            if (user) {
                res.render('perfil/index', {user: user, currentPage: "home"});
            } else {
                res.redirect('/login');
            }
        });
    },
    show_register: function (req, res) {
        res.render('home/register', {message: req.flash('error'), currentPage: "home"});
    },
    logout: function (req, res) {
        req.session.destroy();
        res.redirect('/login');
    }
};