module.exports = {
    index: function (req, res) {
        Meeting.find({}, null, {sort: {'date': -1}}, function (err, meetings) {
            res.render('meetings/list', {
                meetings: meetings,
                error: req.flash('error'),
                success: req.flash('success'),
                currentPage: "encontros"
            });
        });
    },
    new: function (req, res) {
        res.render('meetings/register', {currentPage: "encontros"});
    }
    ,
    save: function (req, res) {
        if (!req.body.presence)
            req.body.presence = [];
        Meeting.findOneAndUpdate({date: req.body.date}, req.body, {upsert: true}, function (err, meeting) {
            if (err) {
                req.flash("error", err);
                console.log(err);
                res.redirect('/encontros');
            } else {
                req.flash("success", "Salvo com sucesso!");
                res.redirect('/encontros/' + meeting._id);
            }
        });
    },
    show: function (req, res) {
        Meeting.findById(req.params.id, function (err, meeting) {
            if (err) {
                console.log(err);
                req.flash('error', 'Ocorreu um erro');
                res.redirect('/encontros');
            } else {
                Teenager.find({_id: {$in: meeting.presence}}, function (err, teens) {
                    if (err) {
                        console.log(err);
                        req.flash('error', 'Ocorreu um erro');
                        res.redirect('/encontros');
                    } else {
                        res.render('meetings/register', {
                            meeting: meeting,
                            currentPage: "encontros",
                            error: req.flash('error'),
                            success: req.flash('success'),
                            teens: teens
                        });
                    }
                });
            }
        });
    },
    delete: function (req, res) {
        Meeting.remove({_id: req.params.id}, function (err) {
            if (err) req.flash("error", err.message);
            else req.flash("success", "Registro Excluído com sucesso");
            res.redirect('/encontros');
        });
    }
};