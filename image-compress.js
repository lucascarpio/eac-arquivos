var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    imageResize = require('gulp-image-resize'),
    rename = require("gulp-rename"), baseImage = "";

module.exports = function (imageUrl) {
    gulp.src(imageUrl).pipe(imagemin({optimizationLevel: 5}))
        .pipe(rename(function (path) {
            baseImage = imageUrl.substring(imageUrl.lastIndexOf("/") + 1, imageUrl.lastIndexOf("."))
            path.basename = baseImage + "_og";
        }))
        .pipe(gulp.dest('public/images'))
        // save 300 x 200
        .pipe(imageResize({
            width: 300,
            height: 200,
            crop: true
        }))
        .pipe(rename(function (path) {
            path.basename = baseImage + "_300";
        }))
        .pipe(gulp.dest('public/images'))
        // save 120 x 120
        .pipe(imageResize({
            width: 120,
            height: 120,
            crop: true
        }))
        .pipe(rename(function (path) {
            path.basename = baseImage + "_120";
        }))
        .pipe(gulp.dest('public/images'))
        // save 48 x 48
        .pipe(imageResize({
            width: 48,
            height: 48,
            crop: true
        }))
        .pipe(rename(function (path) {
            path.basename = baseImage + "_48";
        }))
        .pipe(gulp.dest('public/images'));
};