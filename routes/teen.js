var router = require('express').Router();
var teen = require('../controllers/teen');
var editor = require('path').resolve('../editor.js');

module.exports = function (app) {
    router.get('/', teen.index);
    router.get('/novo', teen.new);
    router.get('/por-termo/:term', teen.getByTerm);
    router.get('/:id', teen.show);
    router.post('/salvar', teen.save);

    return router;
};