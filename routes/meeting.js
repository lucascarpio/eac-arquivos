var router = require('express').Router();
var meeting = require('../controllers/meeting');

module.exports = function (app) {
    router.get('/', meeting.index);
    router.get('/novo', meeting.new);
    router.post('/salvar', meeting.save);
    router.get('/:id',meeting.show);
    router.get('/:id/excluir',meeting.delete);
    return router;
};