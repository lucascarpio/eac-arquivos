var router = require('express').Router();
var user = require('../controllers/user');

module.exports = function (app) {
    router.get('/', user.index);
    router.get('/novo', user.new);
    router.get('/:id', user.show);
    router.post('/salvar', user.save);

    return router;
};