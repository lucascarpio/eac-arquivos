var router = require('express').Router();
var home = require('../controllers/home');

module.exports = function (passport) {
    router.get('/', home.index);
    router.get('/login', home.login);
    router.post('/entrar', passport.authenticate('login', {
        failureRedirect: '/login',
        successRedirect: '/',
        failureFlash: true
    }));

    return router;
};