const KEY = 'keys.sid', SECRET = 'EAC2015';
var express = require('express'),
    path = require('path'),
    cookieParser = require('cookie-parser')(SECRET),
    store = new (require('express-session')).MemoryStore(),
    sessOpts = {secret: SECRET, key: KEY, store: store, saveUninitialized: true, resave: true},
    bodyParser = require('body-parser'),
    session = require('express-session')(sessOpts),
    multer = require('multer'),
    logger = require('morgan'),
    methodOverride = require('method-override'),
    debug = require('debug')('ntalk-structured'),
    app = express(),
    server = require('http').createServer(app),
//io = require('socket.io').listen(server),
    mongoose = require('mongoose'),
    passport = require('./passportsConfig'),
    flash = require('express-flash');
//    googleDrive = require('./google-drive-api');
//var phantom = require('phantom');

global.db = mongoose.connect('mongodb://localhost:27017/eac');
global.Teenager = require('./models/Teenager')(app);
global.User = require('./models/User')(app);
global.Meeting = require('./models/Meeting')(app);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method;
        delete req.body._method;
        return method;
    }
}));
app.use(express.Router());
app.use(cookieParser);
app.use(session);
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());
app.use(multer({dest: './public/images/'}));
app.use(passport.initialize());
app.use(passport.session());


function passportAuthenticated(req, res, next) {
    if (!req.session.passport.user)
        res.redirect('/login');
    next();
}

var home = require('./routes/home')(passport),
    users = require('./routes/user')(app),
    teens = require('./routes/teen')(app),
    meetings = require('./routes/meeting')(app);

app.use('/', home);
app.use('/usuarios', passportAuthenticated, users);
app.use('/adolescentes', passportAuthenticated, teens);
app.use('/encontros', passportAuthenticated, meetings);

app.use(function (req, res, next) {
    res.status(404);
    res.render('not-found');
});

if (app.get('env') === 'development') {

    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    console.log(err);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;

server.listen(3000, function () {
    console.log('Express server listening on port ' + server.address().port);
    debug('Express server listening on port ' + server.address());
});
/*phantom.create(function (ph) {
 ph.createPage(function (page) {
 page.open("file:///home/makros/workspace/copycat-alliar/copycat-alliar-service/services/baseFile.html", function (status) {
 page.render('google.pdf', function () {
 console.log('Page Rendered');
 ph.exit();
 });
 });
 });
 });*/
